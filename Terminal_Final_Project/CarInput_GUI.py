from tkinter import *

##This page is the 2nd module in our GUI that requests more information about currently owned car.
##All members of terminal worked on each part of the code.



root = Tk()


root.title("Basic Car Info")


Label(root, text="Year: ", fg="LimeGreen").grid(row=8)

Year = Entry(root)
Year.grid(row=8, column=1)

Label(root, text="Make: ", fg="LimeGreen").grid(row=9)
Make = Entry(root)
Make.grid(row=9, column=1)

Label(root, text="Model:", fg="LimeGreen").grid(row=10)
Model = Entry(root)
Model.grid(row=10, column=1)

Label(root, text="Trim:", fg="LimeGreen").grid(row=11)
Style = Entry(root)
Style.grid(row=11, column=1)

Label(root, text="Color:", fg="LimeGreen").grid(row=12)
Color = Entry(root)
Color.grid(row=12, column=1)

Label(root, text="Mileage:", fg="LimeGreen").grid(row=13)
Mileage = Entry(root)
Mileage.grid(row=13, column=1)

Label(root, text="Overall Condition 1:3 Enter 1 for Excellent", fg="LimeGreen").grid(row=14)
Condition = Entry(root)
Condition.grid(row=14, column=1)

Label(root, text="Exterior Condition: 1:3", fg="LimeGreen").grid(row=15)
Exterior = Entry(root)
Exterior.grid(row=15, column=1)

Label(root, text="Interior Condition: 1:3", fg="LimeGreen").grid(row=16)
Interior = Entry(root)
Interior.grid(row=16, column=1)

Label(root, text="Engine Type:", fg="LimeGreen").grid(row=17)
Engine = Entry(root)
Engine.grid(row=17, column=1)

Label(root, text="Drivetrain:", fg="LimeGreen").grid(row=18)
Drivetrain = Entry(root)
Drivetrain.grid(row=18, column=1)

Label(root, text="Brakes Condition:  1:3", fg="LimeGreen").grid(row=19)
Brakes = Entry(root)
Brakes.grid(row=19, column=1)

Label(root, text="Wheel Condition:  1:3", fg="LimeGreen").grid(row=20)
Wheels = Entry(root)
Wheels.grid(row=20, column=1)

Label(root, text="Tires Condition:  1:3", fg="LimeGreen").grid(row=21)
Tires = Entry(root)
Tires.grid(row=21, column=1)

def Send():
    import CollectionOfClasses

    CollectionOfClasses.newCar_Info(Year.get(), Model.get(), Make.get(), Style.get(),
                                    Color.get(), Mileage.get(), Condition.get(), Exterior.get(),
                                    Interior.get(), Engine.get(), Drivetrain.get(), Brakes.get(), Wheels.get(), Tires.get())
    Exit()


btnNext = Button(root, text="Next"
                  ,command=lambda: Send(),
                  highlightbackground="blue").grid(row=22)



def Exit():
    root.destroy()

root.mainloop()
