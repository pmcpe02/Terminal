##Place holder for all entered values.

class Car(object):
    model=" "
    make=" "
    style=" "
    color= " "
    condition=" "
    exterior=" "
    interior=" "
    engin=" "
    drivetrain=" "
    brakes= " "
    wheels=" "
    tires = " "
    year=2012
    mileage = 3000

    def __init__(self):
        pass

    def __init__(self, year, model, make, style, color, mileage, condition, exterior, interior, engin, drivetrain,
                 brakes, wheels, tires):
        self.setYear(year)
        self.setModel(model)
        self.setMake(make)
        self.setStyle(style)
        self.setColor(color)
        self.setmileage(mileage)
        self.setCondition(condition)
        self.setExterial(exterior)
        self.setInteror(interior)
        self.setEngin(engin)
        self.setDriveTrain(drivetrain)
        self.setBrakes(brakes)
        self.setWheels(wheels)
        self.setTires(tires)

    def setYear(self, y):
        self.year = y

    def setModel(self, models):
        self.model = models

    def setMake(self, makes):
        self.make = makes

    def setStyle(self, styles):
        self.style = styles

    def setColor(self, colors):
        self.color = colors

    def setmileage(self, mileages):
        self.mileage = mileages

    def setCondition(self, con):
        self.condition = con

    def setExterial(self, exteriors):
        self.exterior = exteriors

    def setInteror(self, interiors):
        self.interior = interiors

    def setEngin(self, engins):
        self.engin = engins

    def setDriveTrain(self, drivetrains):
        self.drivetrain = drivetrains

    def setBrakes(self, brakess):
        self.brakes = brakess

    def setWheels(self, wheelss):
        self.wheels = wheelss

    def setTires(self, tiress):
        self.tires = tiress

    def getYear(self):
        return self.year

    def getModel(self):
        return self.model

    def getMake(self):
        return self.make

    def getStyle(self):
        return self.style

    def getColor(self):
        return self.color

    def getCondition(self):
        return self.condition

    def getExterial(self):
        return self.exterior

    def getmileage(self):
        return self.mileage

    def getInteror(self):
        return self.interior

    def getEngin(self):
        return self.engin

    def getDriveTrain(self):
        return self.drivetrain

    def getBrakes(self):
        return self.brakes

    def getWheels(self):
        return self.wheels

    def getTires(self):
        return self.tires

    # def toStringName(self):
    #     return self.gName
    #
    # def toStringAddress(self):
    #     return self.gAddress
