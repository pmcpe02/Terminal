##this is our 3rd screen of our GUI. This  is where more indepth information about your car comes into play.

from tkinter import *
root=Tk()

root.title("Car Statistics")
Label(root, text="History:  ", fg="Black").grid(row=23)

Label(root, text="Serviced regularly Y or N :  ", fg="LimeGreen").grid(row=24)
Label(root, text="Dealer Serviced Y or N : ", fg="LimeGreen").grid(row=25)
Label(root, text="Maintain  History: ", fg="Black").grid(row=26)
Label(root, text="Poor, Good, Or Excellent? Enter 1 For Excellent", fg="LimeGreen").grid(row=27)
Label(root, text="Warranty: ", fg="Black").grid(row=28)
Label(root, text="Is it still under Manufacture warranty? Y or N : ", fg="LimeGreen").grid(row=29)
Label(root, text="Crashes and Repairs HX : ", fg="Black").grid(row=30)
Label(root, text="Enter # of Crashes or major Repairs", fg="LimeGreen").grid(row=31)
Label(root, text="Part Type: ", fg="Black").grid(row=32)
Label(root, text="Where the repaired / replaced parts original or refurbished? : 1=OEM : 2=WholeSale", fg="LimeGreen").grid(row=33)
Label(root, text="Damage HX : ", fg="Black").grid(row=34)
Label(root, text="Has there been any Major disaster damage? (Fire, Water) enter False for NO disasters: ", fg="LimeGreen").grid(row=35)
Label(root, text="Car Modifications: ", fg="Black").grid(row=36)
Label(root, text="Have you made changes to the car after purchase?: ", fg="LimeGreen").grid(row=37)
Label(root, text="Car Present Condition: ", fg="Black").grid(row=38)
Label(root, text="What is the car's present condition(Enter Poor,Good or Excellent)?: ", fg="LimeGreen").grid(row=39)
Label(root, text="List any known problems?: ", fg="LimeGreen").grid(row=40)

Service              =           Entry(root)
Dealer               =           Entry(root)
PoorGoodExcellent      =   Entry(root)
FollowUp               =           Entry(root)
CrashesOrRepairs     =   Entry(root)
OrigOrRefurb           =         Entry(root)
DisasterDamage     =       Entry(root)
CarChanges      =           Entry(root)
Condition          =            Entry(root)
Problems           =             Entry(root)




Service.grid(row=24, column=1)
Dealer.grid(row=25, column=1)
PoorGoodExcellent.grid(row=27, column=1)
FollowUp.grid(row=29, column=1)
CrashesOrRepairs.grid(row=31, column=1)
OrigOrRefurb.grid(row=33, column=1)
DisasterDamage.grid(row=35, column=1)
CarChanges.grid(row=37, column=1)
Condition.grid(row=39, column=1)
Problems.grid(row=40, column=1)


def Send():

    import CollectionOfClasses

    CollectionOfClasses.newcar_Stats(

        Service.get(),
        Dealer.get(),
        PoorGoodExcellent.get(),
        FollowUp.get(),
        CrashesOrRepairs.get(),
        OrigOrRefurb.get(),
        DisasterDamage.get(),
        CarChanges.get(),
        Condition.get(),
        Problems.get(),
    )

    Exit()




btnNext = Button(root, text="Finish Input"
                      , command=lambda: Send(),
                      highlightbackground="blue").grid(row=41)
# Button(root, text="Next"
#             ,command=lambda: callback2()).grid(row=41)





def Exit():
    root.destroy()





root.mainloop()