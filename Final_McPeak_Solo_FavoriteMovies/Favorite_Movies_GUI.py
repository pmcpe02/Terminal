# Write programming Code to create a menu of your FAVORITE 6 movies with my FAVORITE Radio Buttons.
#
# How it works:
#
# 1. Use tkinter ( GUI )
#
# 2. Click on the option and it opens up your default browser and goes to the IMDB page for that movie
# ( You can go to IMDB.com and look up your favorite movie )
#
# 3. Example: http://www.imdb.com/title/tt0770828/?ref_=nv_sr_1 ( Man of Steel )
#
# Below is an example: I clicked on the Family Man and boom it took me here.
# Big daddy
# Iron Man
# cady shake
# LIttle Nicky
# Pulp Fictioon
# billy Madison
#
from tkinter import *
import tkinter

import webbrowser
root = Tk()

root.title("Philip McPeak Final Exam") ## changes window name
root.config(bg='green')
favorite_movies_Array = [ ['Big Daddy', 'https://www.imdb.com/title/tt0142342/'],
      ['Iron Man', "https://www.imdb.com/title/tt0371746/"],
      ['caddy Shack', 'https://www.imdb.com/title/tt0080487/'],
      ['Little Nicky', "https://www.imdb.com/title/tt0185431/"],
      ['Pulp FictionMan', "https://www.imdb.com/title/tt0110912/"],
      ['Billy Madison', "https://www.imdb.com/title/tt0112508/"]]


Label(root, text="Select your Favorite Movie", fg="blue").grid(row=0,column=0,columnspan=2)



selected = IntVar()


def Clicked():
    webbrowser.open(favorite_movies_Array[selected.get()][1])

for i in range(len(favorite_movies_Array)):

    Radiobutton(root, text=favorite_movies_Array[i][0],value=i ,variable=selected,command=Clicked).grid(row=i+1)





mainloop()
